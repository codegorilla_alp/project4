<?php

Route::get('/', function () {
    // return view('welcome');
    return redirect('/runners');
});


Route::resource('runners', 'RunnerController');
