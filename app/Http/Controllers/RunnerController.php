<?php

namespace App\Http\Controllers;

use App\Runner;
use Illuminate\Http\Request;

class RunnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     *
     */
    public function index()
    {
        $runners = \App\Runner::all();

        return view ('runners.index', compact('runners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('runners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        Runner::create(
            request()->validate([
                'naam' => ['required', 'min:3', 'max:25'],
                'email' => ['required', 'min:3', 'max:35'],
                'afstand' => [''],
                'aantal' => ['']
            ])
        );

        return redirect('/runners');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Runner  $runner
     * @return \Illuminate\Http\Response
     */
    public function show(Runner $runner)
    {
        return view('runners.show', compact('runner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Runner  $runner
     * @return \Illuminate\Http\Response
     */
    public function edit(Runner $runner)
    {
        return view('runners.edit', compact('runner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Runner  $runner
     * @return \Illuminate\Http\Response
     */
    public function update(Runner $runner)
    {
        $runner->update(request(['naam', 'email', 'afstand', 'aantal']));

        return redirect('/runners');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Runner  $runner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Runner $runner)
    {
        $runner->delete();

        return redirect('/runners');
    }
}
