@extends('/layout')

@section('title' , 'Deelnemers info | Avondvierdaagse')


@section('content')

<h1 class="title" style="margin-bottom:1.5em;">Deelnemer info</h1>


<nav class="level">
        <div class="level-item-left">
                <div>
                  <p class="heading">Deelnemer</p>
                  <p class="title">{{ $runner->naam }}</p>
                </div>
              </div>
        <div class="level-item">
          <div>
            <p class="heading">E-mail adres</p>
            <p class="title">{{ $runner->email }}</p>
          </div>
        </div>
        <div class="level-item">
          <div>
            <p class="heading">Afstand</p>
            <p class="title">{{ $runner->afstand }} Km</p>
          </div>
        </div>
        <div class="level-item">
          <div>
            <p class="heading">Aantal meelopers</p>
            <p class="title">{{ $runner->aantal }}</p>
          </div>
        </div>
 </nav>


<div class="field" style="margin-top:3em;">
    <a href="/runners/{{ $runner->id }}/edit" class="button is-primary">Deelnemer aanpassen / wijzigen</a>
</div>

@endsection
