@extends('/layout')

@section('title' , 'Deelnemers aanpassen | Avondvierdaagse')

@section('content')

<h1 class="title" style="margin-bottom:1.5em;">Pas deelnemer gegevens aan</h1>

<form method="POST" action="/runners/{{ $runner->id }}">

    @method('PATCH')

    @csrf

    <div class="field">
        <label class="label">Naam</label>
        <div class="control">
            <input class="input" name="naam" type="text" placeholder="bijv. Richard Alp" value="{{ $runner->naam }}">
        </div>
    </div>

    <div class="field">
        <label class="label">E-mail</label>
        <div class="control">
            <input class="input" name="email" type="email" placeholder="bijv. richardalp@gmail.com" value="{{ $runner->email }}">
        </div>
    </div>

    <div class="field">
        <label class="label">Afstand in Km</label>
        <input class="input" name="afstand" type="text" placeholder="bijv. 1 of 3" value="{{ $runner->afstand }}">
    </div>

    <div class="field">
        <label class="label">Aantal meelopers</label>
        <input class="input" name="aantal" type="text" placeholder="bijv. 1 of 3" value="{{ $runner->aantal }}">
    </div>

    <div class="field" style="margin-top:1em;">
        <button class="button is-success">Pas deelnemer aan</button>
    </div>

</form>

<form style="margin-top:1em;" action="/runners/{{ $runner->id }}" method="POST">

    @method('DELETE')

    @csrf

    <div class="field">
        <button class="button is-danger">Verwijder deelnemer</button>
    </div>

</form>

<a href="{{ url()->previous() }}" style="margin-top:1em;" class="button">
    Ga terug
</a>


@endsection
