@extends('/layout')

@section('title' , 'Avondvierdaagse')


@section('content')

<h1 class="title" style="margin-bottom:1.5em;">Welkom bij avondvierdaagse</h1>
<table class="table is-striped">
        <thead>
                <tr>
                    <th>Naam</th>
                    <th>Aantal Km</th>
                    <th>Aantal meelopers</th>
                </tr>
         </thead>
    @foreach ($runners as $runner)


            <tbody>
                    <tr>
                        <td><a href="/runners/{{ $runner->id }}">
                            {{ $runner->naam }}
                            </a>
                        </td>
                        <td><a href="/runners/{{ $runner->id }}">
                            {{ $runner->afstand }}
                            </a>
                        </td>
                        <td><a href="/runners/{{ $runner->id }}">
                            {{ $runner->aantal }}
                            </a>
                        </td>
                    </tr>
            </tbody>

    @endforeach
</table>
@endsection
