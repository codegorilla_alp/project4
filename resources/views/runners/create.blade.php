@extends('/layout')

@section('title' , 'Avondvierdaagse')

@section('content')

<h1 class="title" style="margin-bottom:1.5em;">Meld je aan bij avondvierdaagse</h1>

<form method="POST" action="/runners">

    @csrf

    <div class="field">
        <label class="label">Naam</label>
        <div class="control">
        <input class="input {{ $errors->has('naam') ? 'is-danger' : '' }}" name="naam" type="text" placeholder="bijv. Richard Alp" value="{{ old('naam') }}">
        </div>
    </div>

    <div class="field">
        <label class="label">E-mail</label>
        <div class="control">
            <input class="input {{ $errors->has('email') ? 'is-danger' : '' }} " name="email" type="email" placeholder="bijv. richardalp@gmail.com" value="{{ old('email') }}">
        </div>
    </div>

    <div class="field">
        <label class="label">Selecteer afstand in (Km)</label>
        <div class="select">
            <select name="afstand">
                <option>5</option>
                <option>10</option>
                <option>15</option>
            </select>
        </div>
    </div>

    <div class="field">
        <label class="label">Aantal meelopers</label>
        <input class="input" name="aantal" type="text" value="0">
    </div>

    <div class="field">
        <button class="button is-primary">Meld mij aan</button>
    </div>

    @if ($errors->any())
    <div class="notification is-danger">
        <ul>

            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach

        </ul>

    </div>
@endif

</form>




@endsection
