<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title', 'Avondvierdaagse')</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.4.0/css/bulma.css" />
</head>

<body>
    <nav class="nav has-shadow">
        <div class="container">
            <div class="nav-left">
                <a class="nav-item is-tab is-hidden-mobile" href="/runners">Home</a>
                <a class="nav-item is-tab is-hidden-mobile" href="/runners/create">Meld je aan!</a>
            </div>
            <span class="nav-toggle">
                <span></span>
                <span></span>
                <span></span>
            </span>
            <div class="nav-right nav-menu">
                <a class="nav-item is-tab is-hidden-tablet" href="/runners">Home</a>
                <a class="nav-item is-tab is-hidden-tablet" href="/runners/create">Meld je aan!</a>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="notification">

            @yield('content')

        </div>
    </div>
</body>

</html>
